<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Roles;
use Illuminate\Console\Command;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-admin {username} {password} {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Admin User into the project';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        //
    }

    protected function getArguments()
    {

    }
}
