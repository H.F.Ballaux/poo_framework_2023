<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class VerifAccessProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $paramIdUrl = $request->route('id');
        if (session('user_id') == $paramIdUrl or session('admin')){
            return $next($request);
        }
        return redirect()->back()->with('danger', 'Vous ne pouvez pas acceder a cette page');
    }
}
