<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {

        return [
            'username' => [
                'regex:/^[0-9a-zA-Z\-]+$/',
                Rule::unique('users', 'username')->ignore(session('user_id')),
            ],
            'o_password' => ['nullable'],
            'n_password' => ['nullable'],
            'email' => [
                'email',
                Rule::unique('users', 'email')->ignore(session('user_id')),
            ],
        ];
    }
}
