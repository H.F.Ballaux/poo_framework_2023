<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use App\Models\User;
use App\Http\Requests\UpdateRolesRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Rules\UpdatePasswordRule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function set_role(UpdateRolesRequest $request)
    {
        $valid = $request->validated();
        $user = User::find($valid['id']);
        if ($user->role != $valid['role']){
            $user->role = $valid['role'];
            $user->save();
            $alert = 'success';
            $msg = 'Le Role a bien été mis a jour';
        }else{
            $alert = 'info';
            $msg = 'Le Role n\'a pas été modifié';
        }
        return redirect()->back()->with($alert, $msg);
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::all();

        foreach ($users as $user){
            $user->role = Roles::find($user->role)->rolename;
        }

        return view('users.users', ['users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('users.inscription');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUserRequest $request)
    {
        $userValidated = $request->validated();
        $admin = RolesController::getRoleByName('admin');
        $visiteur = RolesController::getRoleByName('visiteur');

        $user = new User;
        $user->role = (User::count() === 0) ? $admin->id : $visiteur->id;
        $user->username = $userValidated['username'];
        $user->email = $userValidated['email'];
        $user->password = Hash::make($userValidated['password']);

        $user->save();

        return redirect()->route('login')->with('success', 'Inscription réussie. Vous pouvez maintenant vous connecter.');
    }

    /**
     * Export a resource in JSON File
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
        $user = User::find(session('user_id'));
        $role = Roles::find($user->role);

        if ($user){
            unset($user->password);
            unset($user->image);
            $user->role = $role->rolename;
        }

        $jsonContent = json_encode($user, JSON_PRETTY_PRINT);

        return Response::make($jsonContent, 200, [
            'Content-Type' => 'application/json',
            'Content-Disposition' => 'attachment; filename=user_'.$user->id.'_'.time().'.json',
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        $user = User::find($id);
        if ($user){
            unset($user->password);
            unset($user->image);
            $user->role = Roles::find($user->role)->rolename;
            $user->updated_at = (empty($user->updated_at))? 'Pas encore de mise a jour' : $user->updated_at;
            return view('users.detail_user', ['user'=>$user]);
        }
        return redirect()->route('home')->with('danger', 'Cet Utilisateur n\'existe pas!');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id)
    {
        return view('users.update_user', ['user' => User::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserRequest $request, int $id)
    {
        $user = User::find($id);
        $validatedData = $request->validated();

        // verification de l'ancien mdp pour authoriser la maj
        if (isset($validatedData['o_password']) && isset($validatedData['n_password'])){
            $request->validate([
                'o_password' => [new UpdatePasswordRule]
            ]);
            // on transmet la valeur de n_password a password, de maniere a ce que l'update s'effectue sur le bon champs de la db
            $user->password = Hash::make($validatedData['n_password']);
        }
        $user->username = $validatedData['username'];
        $user->email = $validatedData['email'];

        $user->save();

        return redirect()->route('detail_user', [$user->id])->with('success', 'Profil mis à jour avec succès !');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        //
    }
}
