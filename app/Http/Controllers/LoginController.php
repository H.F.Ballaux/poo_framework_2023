<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginUserRequest;
use Illuminate\Http\RedirectResponse;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     */
    public function authenticate(LoginUserRequest $request): RedirectResponse
    {
        $credentials = $request->validated();

        if (Auth::attempt($credentials)){
            $user = Auth::user();
            $user->updateLastlogin();
            $admin = RolesController::getRoleByName('admin');
            $isAdmin = ($user->role == $admin->id)? 1: 0;
            session(['user_id' => $user->id, 'admin' => $isAdmin]);
            $request->session()->regenerate();
            return redirect()->route('detail_user', [$user->id])->with('success', 'Bienvenue '.$user->username.', vous etes maintenant connecté! ');
        }

        return back()->withErrors([ 'erreurAuth' =>'Erreur de connexion, Veuillez reessayer '])->onlyInput('username');
    }

    public function getLogForm()
    {
        return view('users.login');
    }

    public function logout(Request $request): RedirectResponse
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/')->with('success', 'Goodbye');
    }
}
