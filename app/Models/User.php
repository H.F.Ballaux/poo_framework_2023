<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements Authenticatable
{
    use HasFactory;

    /**
     * Les champs qui pourront etre modifié dynamiquement dans l'application
     * @var string[]
     */
    protected $fillable = [
        'username', 'email', 'password', 'lastlogin', 'role', 'image',
    ];

    protected $visible = [
        'id','username', 'email', 'lastlogin', 'role', 'created_at', 'updated_at'
    ];

    protected $hidden = [
        'password',
    ];

    public function updateLastlogin()
    {
        $this->lastlogin = now();
        $this->save();
    }

    public function getAuthIdentifierName()
    {
        return 'username';
    }

    public function getAuthIdentifier()
    {
        // TODO: Implement getRememberToken() method.
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        // TODO: Implement getRememberToken() method.
    }

    public function setRememberToken($value)
    {
        // TODO: Implement setRememberToken() method.
    }

    public function getRememberTokenName()
    {
        // TODO: Implement getRememberTokenName() method.
    }
}
