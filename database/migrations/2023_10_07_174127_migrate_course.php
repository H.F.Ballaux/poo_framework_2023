<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $courses = [
            [ 'Base des réseaux', '1351' ],
            [ 'Environnement et technologies du web', '1352' ],
            [ 'SGBD (Système de gestion de bases de données)', '1353' ],
            [ 'Création de sites web statiques', '1354' ],
            [ 'Approche Design', '1355' ],
            [ 'CMS - niveau 1', '1356' ],
            [ 'Initiation à la programmation', '1357' ],
            [ 'Activités professionnelles de formation', '1358' ],
            [ 'Scripts clients', '1359' ],
            [ 'Scripts serveurs', '1360' ],
            [ 'Framework et POO côté Serveur', '1361' ],
            [ 'Projet Web dynamique', '1362' ],
            [ 'Veille technologique', '1363' ],
            [ 'Epreuve intégrée', '1364' ],
            [ 'Anglais UE1', '1783' ],
            [ 'Anglais UE2', '1784' ],
            [ 'Initiation aux bases de données', '1440' ],
            [ 'Principes algorithmiques et programmation', '1442' ],
            [ 'Programmation orientée objet', '1443' ],
            [ 'Web : principes de base', '1444' ],
            [ 'Techniques de gestion de projet', '1448' ],
            [ 'Principes d’analyse informatique', '1449' ],
            [ 'Eléments de statistique', '1755' ],
            [ 'Structure des ordinateurs', '1808' ],
            [ 'Gestion et exploitation de bases de données', '1811' ],
            [ 'Mathématiques appliquées à l’informatique', '1807' ],
            [ 'Bases des réseaux', '1323' ],
            [ 'Projet d’analyse et de conception', '1450' ],
            [ 'Information et communication pro7fessionnelle', '1754' ],
            [ 'Produits logiciels de gestion intégrés', '1438' ],
            [ 'Administration, gestion et sécurisation des réseaux', '1439' ],
            [ 'Projet de développement SGBD', '1446' ],
            [ 'Stage d’intégration professionnelle', '1451' ],
            [ 'Projet d’intégration de développement', '1447' ],
            [ 'Activités professionnelles de formation', '1452' ],
            [ 'Epreuve intégrée de la section', '1453' ]
        ];
        $dataToInsert = collect($courses)->map(function ($cours) {
            return ['coursename' => $cours[0], 'code' => $cours[1]];
        })->toArray();

        DB::table('courses')->insert($dataToInsert);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
