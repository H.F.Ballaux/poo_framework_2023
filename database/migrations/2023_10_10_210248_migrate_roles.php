<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $tableau = ['visiteur', 'etudiant', 'professeur', 'admin'];
        $dataToInsert = collect($tableau)->map(function ($role) {
            return ['rolename' => $role];
        })->toArray();

        DB::table('roles')->insert($dataToInsert);

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
