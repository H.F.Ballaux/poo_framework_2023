# Installation : POO_FRAMEWORK_2023

## Prérequis

- WampServer ou equivalent
- PHP version 8.2
- MySQL version 5.6 ou supérieure.
- Git project: https://gitlab.com/H.F.Ballaux/poo_framework_2023.git


## Installation via Git Repository:

Dans votre dossier 'www' de Wamp64 (ou equivalent selon le serveur utilisé), vous allez y cloner le projet git, via la
commande suivante, dans le terminal du system d'exploitation ou dans Git bash

```
git clone https://gitlab.com/H.F.Ballaux/poo_framework_2023.git
```

Après votre clone, un nouveau dossier portant le nom de votre projet devrait etre crée dans votre emplacement actuelle.
Vous devez simplement y acceder en faisant un ;

```
cd nomduprojet
```

Assurez vous bien d'installer les dépendances nécessaire a laravel pour un bon fonctionnement de l'application
pour ce faire utiliser la commande suivante:
```
composer install
```

Pour continuer il faudra configurer le fichier .env, Nous allons donc faire une copie du fichier .env.example et créer un fichier .env que nous pourrons remplir avec nos parametres de configuration.

```
cp .env.example .env
```

Pour son fonctionnement interne, laravel utilise un systeme de cle d'encryption qui permettent d'acceder a divers éléments capitaux pour l'application, 
pour généré la clé il suffit de mettre dans le terminal, la commande suivante: 

```
php artisan key:generate
```

Il vous faudra ensuite créer une base de données vide qui accueillera notre projet, vous pouvez le faire dans PhpMyAdmin ou un service équivalent.

Configuration du fichier .env, celui ci contient les parametres pour acceder a la DB.
Dans ce fichier, il faudra modifier les champs suivants :

- DB_NAME : Le nom de la base de données  (celle créée dans phpMyadmin)
- DB_USER : Le nom d'utilisateur de la base de données
- DB_PASSWORD : Le mots-de-passes d'utilisateur de la base de données
- DB_HOST : Address d'hébergement de la base de données

Ceci fait il ne vous reste plus qu'a importer les données vers la nouvelles DB via la commande:

```
php artisan migrate
```

Il ne vous reste plus qu'à enregistrer les modifications apportées au fichier et démarrer votre nouveau site a l'address
suivante :

- http://localhost/poo_framework_2023/public

Remplacez 'localhost' par address d'hébergement de la base de données référencée dans le fichier 'config.php'

## Name

Project Framework/POO Cote serveur - Hector Fabio Ballaux

## Description

Examen Framework/POO BES WEB 2

## Author

Hector Fabio Ballaux

## Project status

WIP
