<?php

use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\CourseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () { return view('welcome'); })->name('home');

Route::prefix('/users')->controller(UserController::class)->group(function (){
    Route::get('/export', 'export')->name('export')->middleware('CheckUserStatus');
    Route::post('/role', 'set_role')->name('set_role')->middleware('CheckAdminStatus');
    Route::get('/inscriptions', 'create')->name('inscription');
    Route::post('/inscriptions', 'store')->name('inscriptionValidation');
    Route::get('/', 'index')->name('users')->middleware('CheckUserStatus')->middleware('CheckAdminStatus');
    Route::get('{id}', 'show')->name('detail_user')->middleware('CheckUserStatus')->middleware('VerifAccessProfile');
    Route::get('/update/{id}', 'edit')->name('update_user')->middleware('CheckUserStatus');
    Route::post('/update/{id}', 'update')->name('v_update_user')->middleware('CheckUserStatus');
});


Route::prefix('/login')->controller(LoginController::class)->group(function (){
    Route::get('/', [LoginController::class, 'getLogForm'])->name('login');
    Route::post('/', 'authenticate')->name('login_auth');
    Route::get('/logout', 'logout')->name('logout')->middleware('CheckUserStatus');
});

Route::prefix('/courses')->controller(CourseController::class)->group(function (){
    Route::get('/', 'index')->name('courses');
    Route::get('{id}', 'show')->name('detail_cours');
});






