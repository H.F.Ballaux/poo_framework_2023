@extends('layout.layout')

@section('title', 'Login')

@section('content')
    @error('erreurAuth')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <h1>Login</h1>
    <form method="POST" action="">
        @csrf

        <div class="mb-3">
            <input type="text" name="username" class="form-control" placeholder="Username">
        </div>
        @error('username')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="mb-3">
            <input type="password" name="password" class="form-control" placeholder="Password">
        </div>
        @error('password')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-dark">Log In</button>
    </form>
    <br>
    <p>Pas encore des nôtres? <a href="{{ route('inscription') }}"><em>Rejoins Nous!</em></a></p>
@endsection
