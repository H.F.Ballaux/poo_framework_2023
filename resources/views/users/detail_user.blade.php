@extends('layout.layout')

@section('title', 'Profil '.$user->username)

@section('content')
    <h1>Profil {{ $user->username }}</h1>
    <table class="table table-striped table-secondary">
        <thead class="table table-dark">
            <tr>
                <th>Intitulé</th>
                <th>Valeur</th>
            </tr>
        </thead>
        <tbody>
        @foreach($user->getAttributes() as $key => $value)
            <tr>
                <td>{{ $key }}</td><td>{{ $value }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @if(session('user_id') == $user->id)
        <hr>
        <a href="{{ route('update_user', [$user->id]) }}" class="btn btn-dark"> Update Profile</a>
        <a href="{{ route('export') }}" class="btn btn-dark"> Export Profile</a>
    @endif
    @if(session('admin') && session('user_id') != $user->id)
        <hr>
        <form action="{{route('set_role')}}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{ $user->id }}">
            <label>Student
                <input type="radio" name="role" value="2">
            </label><br>
            <label>Teacher
                <input type="radio" name="role" value="3">
            </label>
            <br><br>
            <input type="submit" value="Changer Role" class="btn btn-dark">
        </form>
    @endif
@endsection
