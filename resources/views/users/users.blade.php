@extends('layout.layout')

@section('title', 'Admin')
@section('content')
    <h1>Les Utilisateurs</h1>
    <table class="table table-striped table-secondary">
        <thead class="table table-dark">
        <tr>
            @foreach($users[0]->getVisible() as $key)
                    <th>{{ $key }}</th>
            @endforeach

        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td><a href="{{ route('detail_user', [$user->id]) }}"> {{ $user->username }} </a></td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->lastlogin }}</td>
                <td>{{ $user->role }}</td>
                <td>{{ $user->created_at }}</td>
                <td>{{ $user->updated_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
