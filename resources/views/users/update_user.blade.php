@extends('layout.layout')

@section('title', 'Update '.$user->username)

@section('content')
    <h1>Mise a jour de : {{ $user->username }}</h1>
    <form action="" method="POST">
        @csrf
        <div class="mb-2">
            <input type="text" class="form-control" name="username" value="{{ $user->username }}"
                   placeholder="{{ $user->username}}">
            @error("username")
            {{ $message }}
            @enderror
        </div>
        <div class="mb-2">
            <input type="password" class="form-control" name="o_password" placeholder="Old Password">
            @error("o_password")
            {{ $message }}
            @enderror
        </div>
        <div class="mb-2">
            <input type="password" class="form-control" name="n_password" placeholder="New Password">
            @error("n_password")
            {{ $message }}
            @enderror
        </div>
        <div class="mb-2">
            <input type="email" class="form-control" name="email" value="{{$user->email}}"
                   placeholder="{{$user->email}}">
            @error("email")
            {{ $message }}
            @enderror
        </div>
        <button type="submit" class="btn btn-dark">Update</button>
    </form>
@endsection
