@extends('layout.layout')

@section('title', 'Inscription')

@section('content')
    <h1>Inscription</h1>
    <form action="" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="mb-2">
            <input type="text" class="form-control" name="username" placeholder="username">
        </div>
        @error("username")
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="mb-2">
            <input type="password" class="form-control" name="password" placeholder="Password">
        </div>
        @error("password")
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="mb-2">
            <input type="email" class="form-control" name="email" placeholder="Email">
        </div>
        @error("email")
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-dark">Inscription</button>
    </form>
@endsection
