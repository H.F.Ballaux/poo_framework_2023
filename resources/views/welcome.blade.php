@extends('layout.layout')

@section('title', 'home')

@section('content')
    <h1>Accueil</h1>
    <table class="table table-striped table-secondary">
        <thead class="table table-dark">
        <tr>
            <th>Intitulé</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Projet POO_FRAMEWORK <br> Examen H.F Ballaux (Laravel)</td>
        </tr>
        </tbody>
    </table>
@endsection
