@extends('layout.layout')

@section('title', 'Les Cours')

@section('content')
    <h1>Les Cours</h1>
    <table class="table table-striped table-secondary">
        <thead class="table table-dark">
        <tr>
            <th>Intitulé</th>
            <th>code</th>
        </tr>
        </thead>
        <tbody>
        @foreach($courses as $course)
            <tr>
                <td><a href="{{ route('detail_cours', [$course->id]) }}"> {{ $course->coursename }} </a></td>
                <td>{{ $course->code }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
