@extends('layout.layout')

@section('title')
    Details Cours
@endsection

@section('content')
    <h1>Detail Utilisateur</h1>

    <table class="table table-striped table-secondary">
        <thead class="table table-dark">
        <tr>
            <th>Intitulé</th>
            <th>Valeur</th>
        </tr>
        </thead>
        <tbody>
        @foreach($course->getAttributes() as $key => $value)
            <tr>
                <td>{{ $key }}</td>
                <td>{{ $value }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
